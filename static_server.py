from flask import Flask
from flask import request
from flask import send_from_directory

app = Flask(__name__)


@app.route('/cryptoshark')
def send_ld():
    print('DOWNLOADED CAP FILE by ' + str(request.remote_addr))
    return send_from_directory("./", "./packetcapture.cap")

if __name__ == '__main__':
    app.run(host="localhost", port=8080)
